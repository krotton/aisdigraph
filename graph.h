/*
 * AISDI - zadanie grafowe
 * Krzysztof Otto
 * styczeń 2014
 */

#include <string>
#include <vector>
#include <queue>

#pragma once

using namespace std;

// There two types of connections between nodes:
// - disjunct nodes have non-zero edge weights (first item),
// but no change count.
// - conjunct have zero weights, but change count equal 1 (second item).
struct Node;

struct Cost
{
    int length;    // -1 means infinity.
    uint changes;

    Cost(int length, uint changes):
        length(length), changes(changes)
    {}

    Cost &operator=(const Cost &another);
    Cost operator+(const Cost &another) const;
};

struct Edge
{
    Cost cost;
    Node *destNode;
};

struct Node
{
    string name;        // Human-readable node name
    vector<Edge> edges; // Edges coming out from this node
    bool isTramNode;    // True if this is a tram node, false otherwise.
    bool isTarget;      // True if this is the target node (there may be two of them).

    Cost cost;          // Current travel cost.
    Node *prevNode;     // Previous node in the path.

    bool visited;       // A flag determining whether the node has been visited.

    Node(string name, Cost cost, bool isTramNode, bool isTarget = false):
        name(name), isTramNode(isTramNode), isTarget(isTarget), cost(cost), prevNode(nullptr), visited(false)
    {}

    // Check if new cost relaxes the path and if so, update it and return true.
    // If it does not, return false.
    bool update(Cost cost, Node *from, uint changeLimit);

    // Add a new edge
    void addEdge(Node *dest, Cost cost);

    // Visit
    void visit()
    {
        visited = true;
    }
};

struct compareNodes
{
    bool operator() (const Node *a, const Node *b)
    {
        return !(a->cost.length != -1 && 
            (a->cost.length < b->cost.length || b->cost.length == -1));
    }
};

// This is the heap of unvisited nodes for use with Dijkstra's algorithm.
extern priority_queue<Node*, vector<Node*>, compareNodes> unvisited;

// A list of pointers to all nodes to enable easy cleanup.
extern vector<Node*> allNodes;

// Helper functions for operating on graph
void readNodes(ifstream &file, string startName, string targetName);
Node *findPath(uint changeLimit);
string printPath(Node *targetNode);
void freeNodes();

