graph: graph.o
	g++ -g --std=c++11 -Wall main.cpp graph.o -o graph

graph.o: graph.h graph.cpp
	g++ -g -c --std=c++11 -Wall graph.cpp -o graph.o

clean:
	rm -f graph.o graph

