#include <fstream>
#include <string>
#include <sstream>
#include <map>

#include "graph.h"

using namespace std;
//
// Update the ordering of the unvisited nodes heap.
void rearrangeUnvisited()
{
    make_heap(const_cast<Node**>(&unvisited.top()),
        const_cast<Node**>(&unvisited.top()) + unvisited.size(),
        compareNodes{}
    );
}

Cost &Cost::operator=(const Cost &another)
{
    length = another.length;
    changes = another.changes;

    return *this;
}

Cost Cost::operator+(const Cost &another) const
{
    if (length == -1 && another.length == -1)
        return Cost{-1, changes + another.changes};

    if (length == -1)
        return Cost{another.length, changes + another.changes};

    if (another.length == -1)
        return Cost{length, changes + another.changes};

    return Cost{length + another.length, changes + another.changes};
}

bool Node::update(Cost cost, Node *from, uint changeLimit)
{
    // Check if the length is relaxed
    if (this->cost.length != -1 && cost.length >= this->cost.length)
        return false;

    // Check if we don't exceed the change limit
    if (cost.changes > changeLimit)
        return false;

    this->cost = cost;
    this->prevNode = from;

    rearrangeUnvisited();

    return true;
}

void Node::addEdge(Node *dest, Cost cost)
{
    edges.push_back(Edge{cost, dest});
}

// Helpers:
int extractName(string &out, string line, int since)
{
    int start = line.find("\"", since);
    int end = line.find("\"", start + 1);

    out = line.substr(start, end - start + 1);

    return end + 2;
}

void createConnection(map<std::pair<string, bool>, Node*> &nodes,
        string from, string to, uint len, bool isTram,
        string startName, string targetName)
{
    // Source node
    Node *srcNode = nullptr;

    bool isSource = from == startName;

    auto key = make_pair(from, isSource || isTram);
    auto nodeIt = nodes.find(key);
    if (nodeIt == nodes.end())
    {
        srcNode = new Node(
            from,
            Cost{isSource? 0 : -1, 0},
            isTram,
            from == targetName
        );

        allNodes.push_back(srcNode);

        nodes[key] = srcNode;
    }
    else
    {
        srcNode = nodeIt->second;
    }

    // Destination node
    Node *dstNode = nullptr;

    isSource = to == startName;

    key = make_pair(to, isSource || isTram);
    nodeIt = nodes.find(key);
    if (nodeIt == nodes.end())
    {
        dstNode = new Node(
            to,
            Cost{isSource? 0 : -1, 0},
            isTram,
            to == targetName
        );

        allNodes.push_back(dstNode);

        nodes[key] = dstNode;
    }
    else
    {
        dstNode = nodeIt->second;
    }

    // Bind the nodes
    srcNode->addEdge(dstNode, Cost{len, 0});
}

// Graph operations:

// Parse the input file and build the graph. DO trust the syntax.
void readNodes(ifstream &file, string startName, string targetName)
{
    map<std::pair<string, bool>, Node*> nodes;

    for (string line; getline(file, line); )
    {
        // Read the values.
        string from, to;
        int since = extractName(from, line, 0);
        since = extractName(to, line, since);
        string rest = line.substr(since, line.length());

        uint tramLen, busLen;
        stringstream ss(rest);
        ss >> tramLen;
        ss >> busLen;

        // First for the tram:
        // - check if tram's source node is already there
        // - if it's not, create it
        // - check if tram's destination node is already there
        // - if it's not, create it
        // - bind the source to the destination
        // Repeat the same routine for the bus.
        if (tramLen)
        {
            createConnection(nodes, from, to, tramLen, true, startName, targetName);
        }

        if (busLen)
        {
            createConnection(nodes, from, to, busLen, false, startName, targetName);
        }
    }

    // Now create change connections.
    // For every pair in the nodes map, find a corresponding pair.
    // Connect the two nodes (with the same names, but different transportation)
    // with edges with zero length and non-zero change count.

    // Moreover: push every node into the unvisited queue.

    for (auto it = nodes.begin(); it != nodes.end(); ++it)
    {
        auto matchingKey = make_pair(it->first.first, !it->first.second);
        auto matching = nodes.find(matchingKey);

        if (matching != nodes.end())
        {
            it->second->addEdge(matching->second, Cost{0, 1});
        }

        unvisited.push(it->second);
    }
}

// Find the path using Dijkstra's algorithm.
Node *findPath(uint changeLimit)
{
    while (!unvisited.empty())
    {
        Node *visited = unvisited.top();
        unvisited.pop();
        visited->visit();

        if (visited->cost.length == -1)
        {
            // No more accessible nodes. Path not found.
            return nullptr;
        }

        if (visited->isTarget)
        {
            // We've reached the target node! Return a pointer to it.
            return visited;
        }

        // Update the distances to the neighbour nodes.
        for (Edge &edge: visited->edges)
        {
            // Only travel to unvisited nodes.
            if (edge.destNode->visited)
                continue;

            Cost newCost = visited->cost + edge.cost;
            edge.destNode->update(newCost, visited, changeLimit);
        }
    }

    return nullptr;
}

// Traverse the path from target to source and return the output string.
string printPath(Node *targetNode)
{
    stringstream out;
    out << targetNode->cost.changes << endl;

    vector<string> lines;

    Node *prev = targetNode->prevNode, *curr = targetNode;
    while (prev != nullptr)
    {
        // Ignore edges between bus and tram stops.
        if (curr->name != prev->name)
        {
            stringstream line;
            line << prev->name << " " <<
                curr->name << " " <<
                (curr->isTramNode? "T " : "A ") <<
                curr->cost.length - prev->cost.length;

            lines.push_back(line.str());
        }

        curr = prev;
        prev = prev->prevNode;
    }

    for (auto it = lines.rbegin(); it != lines.rend(); ++it)
    {
        out << *it << endl;
    }

    return out.str();
}

// Free the memory taken by the nodes.
void freeNodes()
{
    for (Node *n : allNodes)
    {
        delete n;
    }
}

