#include <iostream>
#include <fstream>

#include "graph.h"

using namespace std;

// Init shared globals;
priority_queue<Node*, vector<Node*>, compareNodes> unvisited;
vector<Node*> allNodes;

int main(int argc, char *argv[])
{
    if (argc != 3)
    {
        cout << "Too few parameters!" << endl <<
            "Usage: " << argv[0] << " <infile> <outfile>" << endl;

        return 1;
    }

    // Constraints
    uint changeLimit;
    string startName, targetName;

    // Parse the input file
    {
        ifstream file(const_cast<const char*>(argv[1]));

        if (!file.good())
        {
            cout << "[!] No such file: " << argv[1] << endl;
            return 2;
        }

        string t;
        getline(file, t);
        changeLimit = stoi(t);

        getline(file, startName);
        getline(file, targetName);

        readNodes(file, startName, targetName);
    }

    // Look for the shortest path meeting our constraints
    Node *targetNode = findPath(changeLimit);

    // Write to the output file
    {
        ofstream file(const_cast<const char*>(argv[2]));

        if (!file.good())
        {
            cout << "[!] Could not create nor open file: " << argv[2] << endl;
            freeNodes();
            return 3;
        }

        // If no path was found, inform the user
        if (targetNode == nullptr)
        {
            file << "Nie znaleziono trasy!" << endl;
        }

        // However if some path was found, reverse iterate the path and print it.
        else
        {
            file << printPath(targetNode);
        }
    }

    freeNodes();
    return 0;
}

